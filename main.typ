#import "packs.typ"

#show: packs.doc.with(
  authors  : [Autori#packs.thanks("email.degli.autori@cusu.it")],
  date     : "Data",
  title    : "Titolo",
  subtitle : "Sottotitolo",
  abstract : [Un abstract lunghissimo. #lorem(105)],
)

#outline()

Testo

= Titolo di una sezione
Contenuto di una sezione. #lorem(215)
= Titolo di una seconda sezione
Contenuto di una seconda sezione. #lorem(100)
== Titolo di una subsection
Contenuto di una subsection. #lorem(50)
=== Titolo di una sottosottosezione
Contenuto di una subsubsection. #lorem(50)
== Titolo di una subsection
Contenuto di una subsection. #lorem(100)
=== Titolo di una sottosottosezione
Contenuto di una subsubsection. #lorem(50)
=== Titolo di una sottosottosezione
Contenuto di una subsubsection. #lorem(50)
==== Titolo di una sub#super[3]section
Contenuto di una sub#super[3]section. #lorem(60)
