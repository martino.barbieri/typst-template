// Pacchetti
#import "@preview/physica:0.9.2": *
#import "@preview/cetz:0.3.1"

// Cose matematicose
#show: super-T-as-transpose
#let mapsto = math.arrow.bar
#let wedge = math.and

#let otimes = math.times.circle
#let oplus = math.plus.circle

#let simeq = math.tilde.eq
#let pm = math.plus.minus
#let mp = math.minus.plus

#let slashed(body) = math.cancel(angle: 15deg, body)

// Grafica
#let grechina(size) = {
  cetz.canvas({
    import cetz.draw: *
    scale(size)
    let add(x,y) = {(x.at(0)+y.at(0),x.at(1)+y.at(1))}
    let (a1,b1,c1,d1) = ((0.0,0.0),(0.7,0.0),(0.2,0.2),(0.5,0.2))
    let (a2,b2,c2,d2) = ((0.7,0.0),(1.7,0.0),(1.0,-.3),(1.4,-.3))
    let (a3,b3,c3,d3) = ((0.77,0),(1.77,0),(1.07,0.3),(1.47,0.3))
    let (a4,b4,c4,d4) = ((1.77,0),(2.47,0),(1.97,-.2),(2.27,-.2))

    let foo(ds) = {
      merge-path(stroke: none, fill: black, {
        bezier(a1,b1,c1,d1)
        bezier(a2,b2,c2,d2)
        line(b2,add(b2,ds))
        bezier(add(b2,ds),add(a2,ds),add(d2,ds),add(c2,ds))
        bezier(add(b1,ds),add(a1,ds),add(d1,ds),add(c1,ds))
        line(add(a1,ds),a1)
      })
      merge-path(stroke: none, fill: black, {
        bezier(a3,b3,c3,d3)
        bezier(a4,b4,c4,d4)
        line(b4,add(b4,ds))
        bezier(add(b4,ds),add(a4,ds),add(d4,ds),add(c4,ds))
        bezier(add(b3,ds),add(a3,ds),add(d3,ds),add(c3,ds))
        line(add(a3,ds),a3)
      })
    }
    foo((0.04,0.04))
    translate((0.08,0.08))
    foo((0.06,0.06))
  })
}

// Thanks
#let thanks(mail) = {
  footnote(link("mailto:"+mail))
}

// Template
#let doc(
  authors   : none,
  date      : none,
  title     : none,
  subtitle  : none,
  abstract  : none,
  body
) = {
  set page(header-ascent: 12%, header: context {
    if counter(page).at(here()).last() != 1 {
      let select = selector(heading.where(level: 1)).before(here())
      let elems = query(
        select,
      )
      let sec = none
      if elems == () {
        sec = none
      } else {
        sec = {
          [_Sez._ ]
          if elems.last().outlined {
            counter(select).display()
            " — "
          }
          smallcaps(elems.last().body)
        }
      }
      place(bottom, line(length: 100%))
      align(bottom, {grid(columns: (4fr,1fr),
        align(left, sec),
        align(right, {box(grechina(0.35)); " ";counter(page).display()})
      ); v(0.4em)})
    }})
  set heading(
    numbering : "1.1.i.",
  )
  show heading: hd => {
    parbreak()
    if hd.level == 1 {smallcaps({
      if hd.outlined {
        set text(size: 1.1em)
        "sez. "; context {counter(heading).at(here()).last()} ; " "
      }
      set text(size: 1.3em)
      box(width: 5%,line(start: (0em,-0.2em),length: 100%,stroke:2pt))
      " "; hd.body; " "
      box(width: 1fr, line(start: (0em,-0.2em),length: 100%,stroke:2pt))
      parbreak()
      })} else if hd.level == 2 {move(dx: -1.8em, {
      if hd.outlined {
        counter(heading).display(); h(1em)
      }
      hd.body;
    })} else {
      h(-0.9em); "§ "
      box({
        hd
      })
      " — "
    }
  }
  set outline(
    title     : "Indice",
    indent    : 1.5em,
    depth     : 3,
    fill      : repeat(" . "),
  )
  show outline.entry.where(
    level: 1
  ): it => {
    v(12pt, weak: true)
    strong(it)
  }
  set text(
    lang      : "it",
    size      : 11pt,
    font      : "Libertinus Serif",
  )
  set par(
    justify   : true
  )
  align(center,{
    text(size: 2.5em, title)
    linebreak()
    smallcaps(text(size: 1.2em, subtitle))
    if authors != none or date != none {v(1em)}
    authors
    if authors != none and date != none {v(0.5em)}
    date
    if abstract != none {
      v(1em)
      text(weight: "bold", "Sommario")
      v(0.2em)
      box(width: 80%, align(left, abstract))
      v(0.7em)
    }
  })
  body
}
